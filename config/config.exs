# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :budget, BudgetWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Jm6hD1rwvuF2QF2OIRGx1cMC/2oj5yQEDAKyPRS0A2uDTWBy1mn6BjmQRVszY6v6",
  render_errors: [view: BudgetWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Budget.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
