defmodule BudgetWeb.VersionView do
  use BudgetWeb, :view

  def render("index.json", %{version: version}) do
    %{version: version}
  end
end
