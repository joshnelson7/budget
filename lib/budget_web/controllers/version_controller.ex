defmodule BudgetWeb.VersionController do
  use BudgetWeb, :controller

  action_fallback BudgetWeb.FallbackController

  def index(conn, _params) do
    render(conn, "index.json", version: "0.0.1")
  end

end
