import "phoenix_html";
import React from "react";
import ReactDOM from "react-dom";

import Budget from "./components/Budget"
ReactDOM.render(
  <Budget/>,
  document.getElementById("budget-app")
)
