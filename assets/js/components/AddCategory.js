import React from 'react';

export default class AddCategory extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      amount: 0
    }

    this.handleClick = this.handleClick.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleAmountChange = this.handleAmountChange.bind(this);
  }

  handleClick() {
    if (!this.state.name) {
      alert('Please enter a category name');
      return;
    }
    this.props.onClick(this.state);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleAmountChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState( {
      amount: value,
      remaining: value
    })
  }

  render() {
    return (
        <div className="add-category">
          <h4>Add Category</h4>
          <div className="input-form">Name: <input type="text" name="name"
            value={this.state.name} onChange={this.handleInputChange}/></div>
          <div className="input-form">Amount: <input type="text" name="amount"
            value={this.state.amount} onChange={this.handleAmountChange}/></div>
          <div className="input-form">
            <button onClick={this.handleClick}>Add</button>
          </div>
        </div>
      );
  }

}
