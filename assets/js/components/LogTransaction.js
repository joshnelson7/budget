import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import Dropdown from 'react-dropdown';

export default class LogTransaction extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedDate: moment(),
      date: moment().format('MM/DD/YYYY'),
      selectedCategory: '',
      category: '',
      name: '',
      amount: 0
    }

    this.handleClick = this.handleClick.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleDateInputChange = this.handleDateInputChange.bind(this);
    this.handleCategoryInputChange = this.handleCategoryInputChange.bind(this);
  }

  handleClick() {
    if (!this.state.category) {
      alert('Please select a category.  You may need to create one first');
      return;
    }
    this.props.onClick(this.state);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleDateInputChange(date) {
    this.setState({
      selectedDate: date,
      date: date.format('MM/DD/YYYY')
    });
  }

  handleCategoryInputChange(category) {
    this.setState({
      selectedCategory: category.value,
      category: category.value
    })
  }

  render() {
    return (
        <div className="add-transaction">
          <h4>Log Transaction</h4>
          <div className="input-form">
            <Dropdown
              options={this.props.categoryNames}
              onChange={this.handleCategoryInputChange}
              value={this.state.selectedCategory}
              placeholder="Select a category"/>
          </div>
          <div className="input-form">
            Amount:&nbsp;
            <input type="text" name="amount"
            value={this.state.amount} onChange={this.handleInputChange}/></div>
          <div className="input-form">
              <DatePicker selected={this.state.selectedDate}
              onChange={this.handleDateInputChange} />
          </div>
          <div className="input-form">
            <button onClick={this.handleClick}>Log</button>
          </div>
        </div>
      );
  }

}
