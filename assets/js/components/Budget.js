import React from 'react';

import AddCategory from './AddCategory';
import CategoryList from './CategoryList';
import LogTransaction from './LogTransaction';
import Transactions from './Transactions';

const uuidv4 = require('uuid/v4');


export default class Budget extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: [],
      categoryNames: [],
      transactions: [],
      version: ''
    }
  }

  componentDidMount() {

    fetch("http://localhost:4000/api/version")
      .then(res => res.json())
      .then((result) => {
          this.setState({
            version: result.version
          });
        },
        (error) => {
          this.setState({
            version: ''
          });
        }
    )
  }

  render() {
    return (
      <div>
        <h1>Budget App</h1>
        <h6>version:  {this.state.version}</h6>
        <div className="inputs">
          <AddCategory
            onClick={(newCategory) => this.addCategory(newCategory)} />
          <LogTransaction
            categoryNames={this.state.categoryNames}
            onClick={(newTransaction) => this.addTransaction(newTransaction)} />
        </div>
        <div className="lists">
        <CategoryList
          categories={this.state.categories} />
        <Transactions
          transactions={this.state.transactions} />
        </div>
      </div>
    )
  }

  addCategory(newCategory) {
    newCategory.id = uuidv4();
    const newCategoryList = this.state.categories.concat(Object.assign({}, newCategory));
    const newCategoryNameList = newCategoryList.map(category => category.name);
    this.setState({
      categories: newCategoryList,
      categoryNames: newCategoryNameList
    })
  }

  addTransaction(newTransaction) {
    newTransaction.id = uuidv4();
    const newCategoryList = this.state.categories.map(category => {
      if (category.name === newTransaction.category) {
        const newCategory = {
          id: category.id,
          name: category.name,
          amount: category.amount,
          remaining: category.remaining - newTransaction.amount
        }
        return newCategory;
      }
      return category;
    });
    const newTransactionList = this.state.transactions.concat(Object.assign({}, newTransaction));
    this.setState({
      transactions: newTransactionList,
      categories: newCategoryList
    })
  }
}
