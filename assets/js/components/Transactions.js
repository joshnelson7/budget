import React from 'react';
import ReactDOM from 'react-dom';
import FilterableTable from 'react-filterable-table';
import CurrencyFormat from 'react-currency-format';

const fields = [
    { name: 'category', displayName: 'Category', inputFilterable: true, sortable: true },
    { name: 'amount', displayName: 'Amount', inputFilterable: true, exactFilterable: true, sortable: true },
    { name: 'date', displayName: 'Date', inputFilterable: true, exactFilterable: true, sortable: true }
];

export default function Transactions(props) {
  return (
    (
      <div className="transactions">
        <h4>Transactions</h4>

        <FilterableTable
          namespace="Transactions"
          initialSort="date"
          data={props.transactions}
          fields={fields}
          noRecordsMessage="There are no transactions to display"
          noFilteredRecordsMessage="No transaction match these filters"
          topPagerVisible={false}
        />
      </div>
    )
  );
}
