import React from 'react';
import CurrencyFormat from 'react-currency-format';

export default function CategoryList(props) {
  return (
    (
      <div className="budget-sidebar">
        <h4>Budget Categories</h4>
        <table className="category-list">
          <thead>
            <tr>
              <th>Name</th>
              <th>Budgeted</th>
              <th>Remaining</th>
            </tr>
          </thead>
          <tbody>
          {props.categories.map(category => (
              <tr key={category.id}>
                <td>{category.name}</td>
                <td>
                  <CurrencyFormat value={category.amount} displayType={'text'} thousandSeparator={true} prefix={'$'}/>
                </td>
                <td>
                  <CurrencyFormat value={category.remaining} displayType={'text'} thousandSeparator={true} prefix={'$'}/>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  );
}
