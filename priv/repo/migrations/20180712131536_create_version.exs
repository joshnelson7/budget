defmodule Budget.Repo.Migrations.CreateVersion do
  use Ecto.Migration

  def change do
    create table(:version) do
      add :version, :string

      timestamps()
    end

  end
end
