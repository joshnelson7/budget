defmodule Budget.InfoTest do
  use Budget.DataCase

  alias Budget.Info

  describe "version" do
    alias Budget.Info.Version

    @valid_attrs %{version: "some version"}
    @update_attrs %{version: "some updated version"}
    @invalid_attrs %{version: nil}

    def version_fixture(attrs \\ %{}) do
      {:ok, version} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Info.create_version()

      version
    end

    test "list_version/0 returns all version" do
      version = version_fixture()
      assert Info.list_version() == [version]
    end

    test "get_version!/1 returns the version with given id" do
      version = version_fixture()
      assert Info.get_version!(version.id) == version
    end

    test "create_version/1 with valid data creates a version" do
      assert {:ok, %Version{} = version} = Info.create_version(@valid_attrs)
      assert version.version == "some version"
    end

    test "create_version/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Info.create_version(@invalid_attrs)
    end

    test "update_version/2 with valid data updates the version" do
      version = version_fixture()
      assert {:ok, version} = Info.update_version(version, @update_attrs)
      assert %Version{} = version
      assert version.version == "some updated version"
    end

    test "update_version/2 with invalid data returns error changeset" do
      version = version_fixture()
      assert {:error, %Ecto.Changeset{}} = Info.update_version(version, @invalid_attrs)
      assert version == Info.get_version!(version.id)
    end

    test "delete_version/1 deletes the version" do
      version = version_fixture()
      assert {:ok, %Version{}} = Info.delete_version(version)
      assert_raise Ecto.NoResultsError, fn -> Info.get_version!(version.id) end
    end

    test "change_version/1 returns a version changeset" do
      version = version_fixture()
      assert %Ecto.Changeset{} = Info.change_version(version)
    end
  end
end
